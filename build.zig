const std = @import("std");
const builtin = @import("builtin");

pub fn build(b: *std.build.Builder) void {
    const mode = b.standardReleaseOptions();
    const target = b.standardTargetOptions(.{});

    const exe = b.addExecutable("App", null);

    // Change this to bring up a Console or not.
    var subsystem = std.Target.SubSystem.Windows;
    if (mode == .Debug) {
        subsystem = std.Target.SubSystem.Console;
    }
    exe.subsystem = subsystem;

    const flags = [_][]const u8{
        "-std=c99"
    };

    if (builtin.os.tag == .windows) {
        exe.addCSourceFile("src/Main.c", &flags);
        exe.linkSystemLibrary("user32");
        exe.linkSystemLibrary("winmm");
        exe.linkSystemLibrary("kernel32");
        exe.linkSystemLibrary("gdi32");
        exe.linkSystemLibrary("shell32");
    }

    exe.linkLibC();
    exe.setBuildMode(mode);
    exe.setTarget(target);
    exe.install();
}
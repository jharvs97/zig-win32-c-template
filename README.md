# C + Win32 template using Zig toolchain
Sample project to start writing Win32 apps in C using the Zig toolchain.

## How to run
1. Clone the repo
2. Get the Zig toolchain from https://ziglang.org
3. Write some code, then:
```
> cd zig-win32-c-template
> zig build
> .\zig-out\bin\App.exe
```